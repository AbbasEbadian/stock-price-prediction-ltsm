
####################
# import Libraries
####################

import tensorflow as tf
from tensorflow.keras import Sequential
from sklearn.preprocessing import MinMaxScaler
from keras.layers import Dense, LSTM
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt


data = pd.read_csv('/home/XAUUSD51.csv', names=["date", "time", "open", "close", "high", "low", "volume"])
# data = dataf.iloc[-2200:-200]

new_data = data.iloc[-2000:]
new_data.index = pd.to_datetime(new_data['date'] + " " + new_data["time"])
# idx = pd.date_range(end=new_data.iloc[-1].name , periods=2000, freq='5T')
new_data = new_data[['close']]
# new_data.index = idx





####################
# Load data
####################

offset = 50
###############################
# Creating train and test sets
###############################

size = len(new_data) *75 // 100
dataset = new_data.values
train = dataset[:size,:]
valid = dataset[size:,:]


###################
# Normalizing Data
###################

scaler = MinMaxScaler(feature_range=(0, 1))
scaled_data = scaler.fit_transform(dataset)


#############################################
# Converting dataset into x_train and y_train
#############################################

x_train, y_train = [], []
for i in range(60+offset,len(train)):
    x_train.append(scaled_data[i-60-offset:i-offset,0])
    y_train.append(scaled_data[i,0])
x_train, y_train = np.array(x_train), np.array(y_train)
x_train = np.reshape(x_train, (x_train.shape[0],x_train.shape[1],1))


##################################
# Train and fit the LSTM network
##################################

model = Sequential()
model.add(LSTM(units=50, return_sequences=True, input_shape=(x_train.shape[1],1)))
model.add(LSTM(units=50))
model.add(Dense(1))
model.compile(loss='mean_squared_error', optimizer='adam')
model.fit(x_train, y_train, epochs=10, batch_size=2, verbose=2)

data2 = pd.read_csv('/home/XAUUSD5.csv', names=["date", "time", "open", "close", "high", "low", "volume"])
data2.index = pd.to_datetime(data2['date'] + " " + data2["time"])
data2 = data2[["close"]].iloc[-1500:-500]


##########################################################
# Predicting values, using past 60,  from the train data
##########################################################

plt.rcParams["figure.figsize"] = (24,6)
inputs = new_data.iloc[len(new_data) - len(valid) - 60 - offset:]
inputs = inputs.values
inputs = inputs.reshape(-1,1)
inputs  = scaler.transform(inputs)
X_test = []
for i in range(60+offset,inputs.shape[0]+offset):
    X_test.append(inputs[i-60-offset:i-offset,0])

X_test = np.array(X_test)
X_test = np.reshape(X_test, (X_test.shape[0],X_test.shape[1],1))
closing_price = model.predict(X_test)
print(len(inputs), len(closing_price))
############################################
# Plotting true data and predicted data
############################################

closing_price = scaler.inverse_transform(closing_price)
sd = new_data.iloc[-len(valid)].name
idx = pd.date_range(start=sd, periods=(len(valid)+offset), freq='5T')
k = pd.DataFrame([0]*(len(valid)+offset),columns=["close"], index=idx)
k[["close"]] = closing_price.ravel()

# rms=np.sqrt(np.mean(np.power((valid-closing_price),2)))
train = new_data.iloc[:size]
valid = new_data.iloc[size:]
# valid['Predictions'] = closing_price
# valid['Predictions'] = valid['Predictions'].shift(-offset)
print(len(valid), len(train))
plt.grid()
plt.yticks(np.linspace(1, 1.25, 24))
plt.plot(data2.iloc[-2500:][["close"]].applymap(lambda x: x-20), c="g")

plt.plot(valid[['close']])
plt.plot(k[['close']], c="r")